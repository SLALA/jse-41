package ru.t1.strelcov.tm.service;

import org.jetbrains.annotations.NotNull;
import ru.t1.strelcov.tm.api.IService;
import ru.t1.strelcov.tm.api.service.IConnectionService;
import ru.t1.strelcov.tm.dto.model.AbstractEntityDTO;

public abstract class AbstractService<E extends AbstractEntityDTO> implements IService<E> {

    @NotNull
    protected final IConnectionService connectionService;

    public AbstractService(@NotNull final IConnectionService connectionService) {
        this.connectionService = connectionService;
    }

}
