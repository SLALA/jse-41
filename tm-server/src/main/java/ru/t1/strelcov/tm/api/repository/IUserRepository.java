package ru.t1.strelcov.tm.api.repository;

import org.apache.ibatis.annotations.*;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.strelcov.tm.api.IRepository;
import ru.t1.strelcov.tm.dto.model.UserDTO;

import java.util.List;

public interface IUserRepository extends IRepository<UserDTO> {

    @NotNull
    @Select("SELECT id, login, password_hash, role, first_name, last_name, middle_name, email, lock FROM tm_user")
    @Results(value = {
            @Result(property = "id", column = "id"),
            @Result(property = "login", column = "login"),
            @Result(property = "passwordHash", column = "password_hash"),
            @Result(property = "role", column = "role"),
            @Result(property = "firstName", column = "first_name"),
            @Result(property = "lastName", column = "last_name"),
            @Result(property = "middleName", column = "middle_name"),
            @Result(property = "email", column = "email"),
            @Result(property = "lock", column = "lock")
    })
    List<UserDTO> findAll();

    @Insert("INSERT INTO tm_user(id, login, password_hash, role, first_name, last_name, middle_name, email, lock)" +
            " VALUES(#{id}, #{login},#{passwordHash}, #{role}, #{firstName}, #{lastName}, #{middleName}, #{email}, #{lock})")
    void add(@NotNull final UserDTO user);

    @Update("UPDATE tm_user" +
            " SET login = #{login}, password_hash = #{passwordHash}, role = #{role}, first_name = #{firstName}, last_name = #{lastName}, middle_name = #{middleName}, email = #{email}, lock = #{lock}" +
            " WHERE id = #{id}")
    void update(@NotNull final UserDTO user);

    @Nullable
    @Select("SELECT id, login, password_hash, role, first_name, last_name, middle_name, email, lock FROM tm_user WHERE id = #{id}")
    @Results(value = {
            @Result(property = "id", column = "id"),
            @Result(property = "login", column = "login"),
            @Result(property = "passwordHash", column = "password_hash"),
            @Result(property = "role", column = "role"),
            @Result(property = "firstName", column = "first_name"),
            @Result(property = "lastName", column = "last_name"),
            @Result(property = "middleName", column = "middle_name"),
            @Result(property = "email", column = "email"),
            @Result(property = "lock", column = "lock")
    })
    UserDTO findById(@Param("id") @NotNull final String id);

    @Delete("DELETE FROM tm_user")
    void clear();

    @Delete("DELETE FROM tm_user WHERE id = #{id}")
    void remove(@NotNull final UserDTO user);

    @Nullable
    @Select("SELECT id, login, password_hash, role, first_name, last_name, middle_name, email, lock FROM tm_user WHERE login = #{login}")
    @Results(value = {
            @Result(property = "id", column = "id"),
            @Result(property = "login", column = "login"),
            @Result(property = "passwordHash", column = "password_hash"),
            @Result(property = "role", column = "role"),
            @Result(property = "firstName", column = "first_name"),
            @Result(property = "lastName", column = "last_name"),
            @Result(property = "middleName", column = "middle_name"),
            @Result(property = "email", column = "email"),
            @Result(property = "lock", column = "lock")
    })
    UserDTO findByLogin(@Param("login") @NotNull String login);

    @Select("SELECT COUNT(1)>0 FROM tm_user WHERE login = #{login}")
    boolean loginExists(@Param("login") @NotNull final String login);

}
