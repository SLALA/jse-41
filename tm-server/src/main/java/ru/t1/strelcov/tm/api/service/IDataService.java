package ru.t1.strelcov.tm.api.service;


public interface IDataService {


    void saveBackup();


    void loadBackup();


    void saveDataBase64();


    void saveDataBinary();


    void saveDataJsonFasterXml();


    void saveDataJsonJAXB();


    void saveDataXmlFasterXml();


    void saveDataXmlJAXB();


    void saveDataYamlFasterXml();


    void loadDataBase64();


    void loadDataBinary();


    void loadDataJsonFasterXml();


    void loadDataJsonJAXB();


    void loadDataXmlFasterXml();


    void loadDataXmlJAXB();


    void loadDataYamlFasterXml();

}
